%global debug_package %{nil}
%global commit e69d4926027274dadb7d89c45964ddafa1edd7f5
%global shortcommit %(c=%{commit}; echo ${c:0:7})
%global commit_date 20210612

Name:          boost-application
Version:       0.4.12^%{commit_date}g%{shortcommit}
Release:       4%{?dist}
Summary:       Boost Application Library

License:       Boost
URL:           https://github.com/mlegenovic/Boost.Application
Source0:       %{url}/archive/%{commit}.tar.gz#/%{name}-%{shortcommit}.tar.gz

%description
Boost.Application provides a application environment, or start point
to any people that want a basic infrastructure to build an system
application on Windows or Unix Variants (e.g. Linux, MacOS).

%package        devel
Summary:        Development files for %{name}

BuildRequires:  gcc-c++
BuildRequires:  cmake
%if 0%{?rhel}
BuildRequires:  boost176-devel
Requires:       boost176-devel
%else
BuildRequires:  boost-devel
Requires:       boost-devel
%endif

%description    devel
Boost.Application provides a application environment, or start point
to any people that want a basic infrastructure to build an system
application on Windows or Unix Variants (e.g. Linux, MacOS).

%prep
%setup -qn Boost.Application-%{commit}

%build
%cmake
%cmake_build

%check
%ctest

%install
%cmake_install

%files devel
%{_includedir}/boost/application.hpp
%{_includedir}/boost/application/

%changelog
* Mon Feb 21 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 0.4.12^20210612ge69d492-4
- xdev-6 build

* Tue Jul  6 2021 Levent Demirörs <levent.demiroers@profidata.com> - 0.4.12^20210612ge69d492-3
- Do not build in source

* Tue Jul  6 2021 Levent Demirörs <levent.demiroers@profidata.com> - 0.4.12-2e69d492
- Change source to fork from mlegenovic
- Build and execute tests

* Tue Jun 29 2021 Levent Demirörs <levent.demiroers@profidata.com> - 0.4.12-182ac3f7
- Remove git as build dependency
